<?php
/**
 * @file
 * Contains miniOrange Customer class.
 */

/**
 * @file
 * This class represents configuration for customer.
 */
namespace Drupal\radius_login;

class miniOrange_Radius_Customer {

    public $email;
    public $phone;
    public $password;
    public $otpToken;
    private $defaultCustomerId;
    private $defaultCustomerApiKey;

    /**
    * Constructor.
    */
    public function __construct( $email, $phone, $password, $otp_token ) {
        $this->email = $email;
        $this->phone = $phone;
        $this->password = $password;
        $this->otpToken = $otp_token;
        $this->defaultCustomerId = "16555";
        $this->defaultCustomerApiKey = "fFd2XcvTGDemZvbw1bcUesNJWEqKbbUq";
    }

    /**
    * Check if customer exists.
    */
    public function checkCustomer() {
        $utilities = new miniOrange_Radius_Utilities();
        if ( !$utilities->isCurlInstalled() ) {
            $utilities->miniOrange_status_message('<a href="http://php.net/manual/en/curl.installation.php">PHP cURL extension</a> is not installed or disabled.', 'error' );
            return 0;
        }

        $url = $utilities->miniOrange_get_constants('CHECK_IF_EXISTS' );
        $ch = curl_init( $url );
        $email = $this->email;

        $fields = array(
            'email' => $email,
        );
        $field_string = json_encode( $fields );

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json', 'charset: UTF - 8',
            'Authorization: Basic',
        ));
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
        $content = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            $utilities->miniOrange_add_loggers( "cURL Error at <strong>checkCustomer</strong> function of <strong>miniOrange_Radius_Customer.php</strong> file: " . curl_error( $ch ), 'ERROR' );
        }
        curl_close( $ch );
        return $content;
    }

    /**
    * Create Customer.
    */
    public function createCustomer() {
        $utilities = new miniOrange_Radius_Utilities();
        if ( !$utilities->isCurlInstalled() ) {
            $utilities->miniOrange_status_message('<a href="http://php.net/manual/en/curl.installation.php">PHP cURL extension</a> is not installed or disabled.', 'error' );
            return 0;
        }
        $url = $utilities->miniOrange_get_constants('ADD' );
        $ch = curl_init( $url );

        $fields = array(
            'companyName' => $_SERVER['SERVER_NAME'],
            'areaOfInterest' => 'DRUPAL 8 Radius Client Module',
            'email' => $this->email,
            'phone' => $this->phone,
            'password' => $this->password,
        );
        $field_string = json_encode( $fields );

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'charset: UTF - 8',
            'Authorization: Basic',
        ));
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
        $content = curl_exec( $ch );

        if (curl_errno($ch)) {
            $utilities->miniOrange_add_loggers( "cURL Error at <strong>createCustomer</strong> function of <strong>miniOrange_Radius_Customer.php</strong> file: " . curl_error( $ch ), 'ERROR' );
        }
        curl_close($ch);
        return $content;
    }

    /**
    * Get Customer Keys.
    */
    public function getCustomerKeys() {
        $utilities = new miniOrange_Radius_Utilities();
        if ( !$utilities->isCurlInstalled() ) {
            $utilities->miniOrange_status_message('<a href="http://php.net/manual/en/curl.installation.php">PHP cURL extension</a> is not installed or disabled.', 'error' );
            return 0;
        }

        $url = $utilities->miniOrange_get_constants('KEY' );
        $ch = curl_init( $url );
        $email = $this->email;
        $password = $this->password;

        $fields = array(
            'email' => $email,
            'password' => $password,
        );
        $field_string = json_encode( $fields );

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'charset: UTF - 8',
            'Authorization: Basic',
        ));
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
        $content = curl_exec($ch);
        if ( curl_errno( $ch ) ) {
            $utilities->miniOrange_add_loggers( "cURL Error at <strong>getCustomerKeys</strong> function of <strong>miniOrange_Radius_Customer.php</strong> file: " . curl_error( $ch ), 'ERROR' );
        }
        curl_close($ch);
        return $content;
    }

    /**
    * Send OTP.
    */
    public function sendOtp() {
        $utilities = new miniOrange_Radius_Utilities();
        if ( !$utilities->isCurlInstalled() ) {
            $utilities->miniOrange_status_message('<a href="http://php.net/manual/en/curl.installation.php">PHP cURL extension</a> is not installed or disabled.', 'error' );
            return 0;
        }
        $url = $utilities->miniOrange_get_constants('CHALLENGE' );
        $ch = curl_init( $url );
        $customer_key = $this->defaultCustomerId;
        $api_key = $this->defaultCustomerApiKey;

        $mo_db_values = $utilities->miniOrange_set_get_configurations( array( 'miniorange_radius_customer_admin_email' ), 'GET' );
        $username = $mo_db_values['miniorange_radius_customer_admin_email'];

        /* Current time in milliseconds since midnight, January 1, 1970 UTC. */
        $current_time_in_millis = round(microtime(TRUE ) * 1000 );

        /* Creating the Hash using SHA-512 algorithm */
        $string_to_hash = $customer_key . number_format($current_time_in_millis, 0, '', '' ) . $api_key;
        $hash_value = hash("sha512", $string_to_hash);

        $customer_key_header = "Customer-Key: " . $customer_key;
        $timestamp_header = "Timestamp: " . number_format($current_time_in_millis, 0, '', '' );
        $authorization_header = "Authorization: " . $hash_value;

        $fields = array(
            'customerKey' => $customer_key,
            'email' => $username,
            'authType' => 'EMAIL',
        );
        $field_string = json_encode( $fields );

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", $customer_key_header,
            $timestamp_header, $authorization_header,
        ));
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
        $content = curl_exec($ch);

        if ( curl_errno( $ch ) ) {
            $utilities->miniOrange_add_loggers( "cURL Error at <strong>sendOtp</strong> function of <strong>miniOrange_Radius_Customer.php</strong> file: " . curl_error( $ch ), 'ERROR' );
        }
        curl_close( $ch );
        return $content;
    }

  /**
   * Validate OTP.
   */
    public function validateOtp( $transaction_id ) {
        $utilities = new miniOrange_Radius_Utilities();
        if ( !$utilities->isCurlInstalled() ) {
            $utilities->miniOrange_status_message('<a href="http://php.net/manual/en/curl.installation.php">PHP cURL extension</a> is not installed or disabled.', 'error' );
            return 0;
        }

        $url = $utilities->miniOrange_get_constants('VALIDATE' );
        $ch = curl_init( $url );

        $customer_key = $this->defaultCustomerId;
        $api_key = $this->defaultCustomerApiKey;

        $current_time_in_millis = round(microtime(TRUE ) * 1000 );
        $string_to_hash = $customer_key . number_format($current_time_in_millis, 0, '', '' ) . $api_key;
        $hash_value = hash("sha512", $string_to_hash);
        $customer_key_header = "Customer-Key: " . $customer_key;
        $timestamp_header = "Timestamp: " . number_format($current_time_in_millis, 0, '', '' );
        $authorization_header = "Authorization: " . $hash_value;

        $fields = array(
          'txId' => $transaction_id,
          'token' => $this->otpToken,
        );

        $field_string = json_encode( $fields );

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", $customer_key_header,
          $timestamp_header, $authorization_header,
        ));
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
        $content = curl_exec($ch);

        if ( curl_errno( $ch ) ) {
            $utilities->miniOrange_add_loggers( "cURL Error at <strong>validateOtp</strong> function of <strong>miniOrange_Radius_Customer.php</strong> file: " . curl_error( $ch ), 'ERROR' );
        }
        curl_close( $ch );
        return $content;
    }
}