<?php
/**
 * @package    miniOrange
 * @subpackage Module
 * @license    GNU/GPLv3
 * @copyright  Copyright 2015 miniOrange. All Rights Reserved.
 *
 *
 * This file is part of miniOrange Radius Login module for Drupal.
 *
 * miniOrange Radius Login module for Drupal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miniOrange Radius Login module for Drupal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with miniOrange SAML module.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Drupal\radius_login;


class miniOrange_Radius_Utilities {

    /**
     * HANDALE ALL THE DATABASE VARIABLE CALLS LIKE SET|GET|CLEAR
     * -----------------------------------------------------------------------
     * @variable_array:
     * FORMAT OF ARRAY FOR DIEFRENT @param
     * SET array( vaviable_name1(key) => value, vaviable_name2(key) => value )
     * GET and CLEAR array( vaviable_name1(value), vaviable_name2(value) )  note: key doesnt matter here
     * -----------------------------------------------------------------------
     * @mo_method:  SET | GET | CLEAR
     * -----------------------------------------------------------------------
     * @return array | void
     */
    public static function miniOrange_set_get_configurations( $variable_array, $mo_method ) {
        if( $mo_method === 'GET' ) {
            $variables_and_values = array();
            $miniOrange_config = \Drupal::config('radius_login.settings');
            foreach ( $variable_array as $variable => $value ) {
                $variables_and_values[$value] = $miniOrange_config->get( $value );
            }
            return $variables_and_values;
        }
        $configFactory = \Drupal::configFactory()->getEditable('radius_login.settings');
        if( $mo_method === 'SET' ) {
            foreach ($variable_array as $variable => $value) {
                $configFactory->set($variable, $value)->save();
            }
            return;
        }
        foreach ($variable_array as $variable => $value) {
            $configFactory->clear($value)->save();
        }
    }

    /**
     * @message: Which you want to show to the user.
     * @msg_type: status = success, error = errors, warning = warnings
     */
    public static function miniOrange_status_message( $message, $msg_type, $repeat = false ) {
        \Drupal::messenger()->addMessage(t( $message ), $msg_type, $repeat );
        return;
    }

    /**
     * @message: Which you want to add in the logger report.
     * @TypeOfLogger = ERROR, EMERGENCY, WARNING, ALERT, CRITICAL, NOTICE, INFO, DEBUG.
     */
    public static function miniOrange_add_loggers( $message, $typeOfLogger ) {
        if( $typeOfLogger === 'ERROR' ) {
            \Drupal::logger('radius_login')->error( $message );
        }
    }

    public static function miniOrange_send_support_query( $values ) {
        $support_response = miniOrange_Radius_Support::sendSupportQuery( $values );
        if( $support_response ) {
            miniOrange_Radius_Utilities::miniOrange_status_message('Thanks for getting in touch! We will get back to you soon.', 'status' );
            return;
        }
        miniOrange_Radius_Utilities::miniOrange_status_message('Error sending support query', 'error' );
        return;
    }

    /**
     * Advertise Network Security module on each tab.
     */
    public static function miniOrange_advertise_network_security( &$form, &$form_state ) {
        global $base_url;

        $form['miniorange_idp_guide_link3'] = array(
            '#markup' => '<div class="mo_radius_table_layout mo_radius_container_2">',
        );
        $form['mo_idp_net_adv'] = array(
            '#markup'=>'<form name="f1">
                <table id="idp_support" class="idp-table" style="border: none;">
                <h3>'. t('Looking for a Drupal Web Security module?') .'</h3>
                    <tr class="mo_ns_row">
                        <th class="mo_ns_image1"><img
                                    src="'.$base_url . '/' . drupal_get_path("module", "radius_login") . '/includes/images/security.jpg"
                                    alt="security icon" height=150px width=44%>
                           <br>
                        <strong>'. t('Drupal Website Security') .'</strong>
                        </th>
                    </tr>
                    <tr class="mo_ns_row">
                        <td class="mo_ns_align">
                            '. t('Building a website is a time-consuming process that requires tremendous efforts. For smooth
                            functioning and protection from any sort of web attack appropriate security is essential and we
                            ensure to provide the best website security solutions available in the market.
                            We provide you enterprise-level security, protecting your Drupal site from hackers and malware.') .'
                        </td>
                    </tr>
                </table>
            </form>'
        );

        self::miniOrange_add_network_security_buttons($form, $form_state );

        return $form;
    }
    public static function miniOrange_add_network_security_buttons( &$form, &$form_state ) {
        $form['miniorange_radius_buttons'] = array(
            '#markup' => '<div class="mo2f_text_center"><b></b>
                          <a class=" mo_radius_button_left" href="https://www.drupal.org/project/security_login_secure" target="_blank">'.t('Download Module').'</a>
                          <b></b><a class=" mo_radius_button_right" href="https://plugins.miniorange.com/drupal-web-security-pro" target="_blank">'.t('Know More').'</a></div></div>',
        );
    }

    /**
     * Check if cURL extension is enabled or not
     * @return int
     */
    public static function isCurlInstalled() {
        if ( in_array('curl', get_loaded_extensions() ) ) {
            return 1;
        }
        return 0;
    }

    /**
     * Append premium tag to the premium features
     */
    public static function miniOrange_add_premium_tag() {
        return '<a href="'.self::miniOrange_get_constants('LICENSING_TAB').'">[Premium]</a>';
    }

    /**
     * @parameter: See the CASE below.
     * @returns string (requested URL) depending upon parameter.
     */
    public static function miniOrange_get_constants( $parameter ) {
        global $base_url;
        $mo_base_url = 'https://login.xecurify.com';
        switch ( $parameter ) {
            case 'BASE_URL':
                return $base_url;
                break;
            case 'CONTACT_US':
                return $mo_base_url . '/moas/rest/customer/contact-us';
                break;
            case 'SEND':
                return $mo_base_url . '/moas/api/notify/send';
                break;
            case 'CHECK_IF_EXISTS':
                return $mo_base_url . '/moas/rest/customer/check-if-exists';
                break;
            case 'ADD':
                return $mo_base_url . '/moas/rest/customer/add';
                break;
            case 'KEY':
                return $mo_base_url . '/moas/rest/customer/key';
                break;
            case 'CHALLENGE':
                return $mo_base_url . '/moas/api/auth/challenge';
                break;
            case 'VALIDATE':
                return $mo_base_url . '/moas/api/auth/validate';
                break;
            case 'LOGS':
                return $base_url . '/admin/reports/dblog';
                break;
            case 'LICENSING_TAB':
                return $base_url . '/admin/config/people/radius_login/radius_upgrade_plans';
                break;
            case 'SUPPORT_TAB':
                return $base_url . '/admin/config/people/radius_login/radius_support';
                break;
            default:
                return '';
        }
    }
	
	public static function mo_get_drupal_core_version() {
        return \DRUPAL::VERSION;
    }
}