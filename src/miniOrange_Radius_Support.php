<?php

namespace Drupal\radius_login;

/**
 * @file
 * This class represents support information for customer.
 */
//use Drupal\radius_login\miniOrange_Radius_Utilities;

class miniOrange_Radius_Support
{
    /**
     * Send support query.
     * @param $values
     * @return bool
     */
    public static function sendSupportQuery( $values ) {
        $modules_info    = \Drupal::service('extension.list.module')->getExtensionInfo('radius_login');
        $modules_version = $modules_info['version'];

        $utilities = new miniOrange_Radius_Utilities();
        $email              = $values['email'];
        $phone_or_plan      = $values['phone_or_plan'];
        $query_or_usecase   = $values['query_or_usecase'];
        $demo_request       = isset( $values['demo_request'] ) ? $values['demo_request'] : '';

        if( $demo_request == 'DEMO' ) {
            $url = miniOrange_Radius_Utilities::miniOrange_get_constants('SEND');
            $ch = curl_init ( $url );

            $mo_db_values = $utilities->miniOrange_set_get_configurations( array('miniorange_radius_customer_id','miniorange_radius_customer_api_key'), 'GET' );

            $customerKey = $mo_db_values['miniorange_radius_customer_id'];
            $apikey      = $mo_db_values['miniorange_radius_customer_api_key'];
            if( $customerKey == '' ) {
                $customerKey = "16555";
                $apikey      = "fFd2XcvTGDemZvbw1bcUesNJWEqKbbUq";
            }

            $currentTimeInMillis = round(microtime(TRUE) * 1000);
            $stringToHash 		 = $customerKey .  $currentTimeInMillis . $apikey;
            $hashValue 			 = hash("sha512", $stringToHash);
            $customerKeyHeader 	 = "Customer-Key: " . $customerKey;
            $timestampHeader 	 = "Timestamp: " .  $currentTimeInMillis;
            $authorizationHeader = "Authorization: " . $hashValue;

            $content='<div >Hello, <br>
                        <br>Company :<a href="'.$_SERVER['SERVER_NAME'].'" target="_blank" >'.$_SERVER['SERVER_NAME'].'</a><br>
                        <br>Plan Name:'.$phone_or_plan.'<br>
                        <br>Email:<a href="mailto:'.$email.'" target="_blank">'.$email.'</a><br>
                        <br>Query:[DRUPAL ' . miniOrange_Radius_Utilities::mo_get_drupal_core_version() . ' RADIUS CLIENT FREE] '. $query_or_usecase.'
                      </div>';

            $fields = array(
                'customerKey'	=> $customerKey,
                'sendEmail' 	=> true,
                'email' 		=> array(
                    'customerKey' 	=> $customerKey,
                    'fromEmail' 	=> $email,
                    'fromName' 		=> 'miniOrange',
                    'toEmail' 		=> 'drupalsupport@xecurify.com',
                    'toName' 		=> 'drupalsupport@xecurify.com',
                    'subject' 		=> "Demo request for " . $phone_or_plan,
                    'content' 		=> $content
                ),
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", $customerKeyHeader,
                $timestampHeader, $authorizationHeader));
        }else {
            $query = '[Drupal ' . miniOrange_Radius_Utilities::mo_get_drupal_core_version() . ' Radius Client Free | ' . $modules_version . '] ' . $query_or_usecase;

            $fields = array(
                'company' => $_SERVER ['SERVER_NAME'],
                'email' => $email,
                'ccEmail' => 'drupalsupport@xecurify.com',
                'phone' => $phone_or_plan,
                'query' => $query,
                'subject' => "Drupal " . miniOrange_Radius_Utilities::mo_get_drupal_core_version() . " Radius Client Free Query | " . $modules_version,
            );

            $url = $utilities->miniOrange_get_constants('CONTACT_US');
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'charset: UTF-8',
                'Authorization: Basic'
            ));
        }
        $field_string = json_encode( $fields );
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
        $content = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            $utilities->miniOrange_add_loggers( "cURL Error at <strong>sendSupportQuery</strong> function of <strong>miniOrange_Radius_Support.php</strong> file: " . curl_error( $ch ), 'ERROR' );
            return FALSE;
        }
        curl_close( $ch );
        return TRUE;
    }
}