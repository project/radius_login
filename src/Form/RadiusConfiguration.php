<?php

namespace Drupal\radius_login\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\radius_login\miniOrange_Radius_Utilities;

class RadiusConfiguration extends FormBase
{
    public function getFormId() {
        return 'miniorange_radius_config_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state) {

        global $base_url;
        $utilities = new miniOrange_Radius_Utilities();
        $variables_and_values = array(
            'miniorange_radius_enable_radius_login',
            'miniorange_radius_server_name',
            'miniorange_radius_server_ip_host',
            'miniorange_radius_radius_port',
            'miniorange_radius_shared_secret',
            'miniorange_radius_authentication_scheme',
            'miniorange_radius_attribute_mapping',
            'miniorange_radius_auto_create_user',
        );

        $mo_db_values = $utilities->miniOrange_set_get_configurations( $variables_and_values, 'GET' );

        $form['miniorange_markup_radius_config_header'] = array(
            '#markup' => '<div class="miniorange_radius_table_layout_main"><div class="miniorange_radius_table_layout_sub miniorange_radius_container">'
        );

        /**
         * Create container to hold @RadiuaServerConfigurations form elements.
         */
        $form['mo_radiua_server_configurations_form'] = array(
            '#type' => 'fieldset',
            '#title' => t( 'Configure Radius Server' ),
            '#attributes' => array( 'style' => 'padding:2% 2% 6% 2%; margin-bottom:2%' ),
        );

        $form['mo_radiua_server_configurations_form']['miniorange_radius_enable_radius_login'] = array(
            '#type' => 'checkbox',
            '#title' => t('Enable Radius Login'),
            '#default_value' => $mo_db_values['miniorange_radius_enable_radius_login'],
            '#description' => t('<strong>Note:</strong> Enabling Radius login will protect your login page by your configured Radius server. Please test if Radius login works with your administrator account in another browser or private window first as <span style="color: red">your default Drupal login will stop working.</span><br><br>'),
            '#prefix' => '<br><hr><br>',
        );

        $form['mo_radiua_server_configurations_form']['miniorange_radius_server_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Radius Name'),
            '#default_value' => $mo_db_values['miniorange_radius_server_name'],
            '#attributes' => array('placeholder' => t('Radius server name'), 'style' => 'width:80%;'),
            '#required' => TRUE
        );

        $form['mo_radiua_server_configurations_form']['miniorange_radius_server_ip_host'] = array(
            '#type' => 'textfield',
            '#title' => t('Radius Server IP / Host'),
            '#default_value' => $mo_db_values['miniorange_radius_server_ip_host'],
            '#attributes' => array('placeholder' => t('Enter IP address of your Radius server'), 'style' => 'width:80%;'),
            '#required' => TRUE
        );

        $form['mo_radiua_server_configurations_form']['miniorange_radius_radius_port'] = array(
            '#type' => 'textfield',
            '#title' => t('Radius Port'),
            '#default_value' => $mo_db_values['miniorange_radius_radius_port'] == '' ? 1812 : $mo_db_values['miniorange_radius_radius_port'],
            '#attributes' => array('placeholder' => t('Radius port number eg. 1812'), 'style' => 'width:80%;'),
            '#description' => t('<strong>Note: </strong> Most of the Radius server have 1812 port number.'),
            '#required' => TRUE
        );

        $form['mo_radiua_server_configurations_form']['miniorange_radius_shared_secret'] = array(
            '#type' => 'textfield',
            '#title' => t('Shared Secret'),
            '#default_value' => $mo_db_values['miniorange_radius_shared_secret'],
            '#attributes' => array('placeholder' => t('Shared secret'), 'style' => 'width:80%;'),
            '#description' => t('<strong>Note: </strong> Enter the same shared secret which you used while configuring the Radius server.'),
            '#required' => TRUE,
        );

        $form['mo_radiua_server_configurations_form']['miniorange_radius_authentication_scheme'] = array(
            '#title' => t('Authentication Scheme:'),
            '#type' => 'radios',
            '#options' => array(
                'pap_authentication' => t('PAP'),
                'chap_md5_authentication' => t('CHAP-MD5 ' . $utilities::miniOrange_add_premium_tag() ),
                'mschap_v1_authentication' => t('MSCHAP v1 ' . $utilities::miniOrange_add_premium_tag() ),
                'eap_mschap_v2_authentication' => t('EAP-MSCHAP v2 ' . $utilities::miniOrange_add_premium_tag() ),
            ),
            '#default_value' => $mo_db_values['miniorange_radius_authentication_scheme'] ? $mo_db_values['miniorange_radius_authentication_scheme'] : 'pap_authentication',
            '#attributes' => array('class' => array('container-inline')),
            '#required' => TRUE,
            '#prefix' => '<br>',
        );

        $form['mo_radiua_server_configurations_form']['miniorange_radius_attribute_mapping'] = array(
            '#title' => t('Attribute Mapping:'),
            '#type' => 'radios',
            '#options' => array(
                'username' => t('Username'),
                'email' => t('Email ' . $utilities::miniOrange_add_premium_tag() ),
                'both' => t('Both (we will try with username first) ' . $utilities::miniOrange_add_premium_tag() ),
            ),
            '#description' => t('<strong>Note: </strong> Radius username will be matched with Drupal username or email or both.'),
            '#default_value' => $mo_db_values['miniorange_radius_attribute_mapping'] ? $mo_db_values['miniorange_radius_attribute_mapping'] : 'username',
            '#attributes' => array('class' => array('container-inline')),
            '#required' => TRUE,
            '#prefix' => '<br>',
        );

        $form['mo_radiua_server_configurations_form']['miniorange_radius_auto_create_user'] = array(
            '#title' => t('Auto Create User:'),
            '#type' => 'radios',
            '#options' => array(
                'allowed' => t('Allowed'),
                'not_allowed' => t('Not Allowed ' . $utilities::miniOrange_add_premium_tag() ),
            ),
            '#description' => t('<strong>Note: </strong>If <strong>"Allowed"</strong> then we will auto create users for successful authentication if users not already exist in <strong>' . $base_url .'</strong>'),
            '#default_value' => $mo_db_values['miniorange_radius_auto_create_user'] ? $mo_db_values['miniorange_radius_auto_create_user'] : 'allowed',
            '#attributes' => array('class' => array('container-inline')),
            '#required' => TRUE,
            '#prefix' => '<br>',
        );

        $form['mo_radiua_server_configurations_form']['miniorange_markup_radius_config_submit'] = array(
            '#type' => 'submit',
            '#button_type' => 'primary',
            '#value' => t('Save Configuration'),
            '#prefix' => '<br><br>',
        );

        $form['mo_radiua_server_configurations_form']['miniorange_markup_radius_config_remove'] = array(
            '#type' => 'submit',
            '#button_type' => 'danger',
            '#value' => t('CLEAR CONFIGURATION'),
            '#submit' => array('::miniOrange_clear_configuration'),
            '#suffix' => '<br><br>',
        );

        $form['mo_radiua_server_configurations_form']['mo_radiua_authentication_protocol'] = array(
            '#markup' => t('<br><hr><hr><br><div><h5>The acronyms for Radius Authentication Protocol types:</h5> 
                     <ul>
                         <li><strong>PAP:</strong> Password Authentication Protocol</li>
                         <li><strong>CHAP:</strong> Challenge Handshake Authentication Protocol</li>
                         <li><strong>MSCHAP:</strong> Microsoft Challenge Handshake Authentication Protocol</li>
                         <li><strong>EAP-MSCHAP:</strong> Extensible Authentication Protocol - Microsoft Challenge Handshake Authentication Protocol</li>
                     </ul> 
                 </div></div>')
        );

        $utilities->miniOrange_advertise_network_security($form, $form_state);

        return $form;
    }

    function submitForm(array &$form, FormStateInterface $form_state) {

        $form_values = $form_state->getValues();
        $variables_and_values = array(
            'miniorange_radius_enable_radius_login'   => $form_values['miniorange_radius_enable_radius_login'] == 1 ? TRUE : FALSE,
            'miniorange_radius_server_name'           => $form_values['miniorange_radius_server_name'],
            'miniorange_radius_server_ip_host'        => trim( $form_values['miniorange_radius_server_ip_host'] ),
            'miniorange_radius_radius_port'           => trim( $form_values['miniorange_radius_radius_port'] ),
            'miniorange_radius_shared_secret'         => $form_values['miniorange_radius_shared_secret'],
            'miniorange_radius_authentication_scheme' => $form_values['miniorange_radius_authentication_scheme'],
            'miniorange_radius_attribute_mapping'     => $form_values['miniorange_radius_attribute_mapping'],
            'miniorange_radius_auto_create_user'      => $form_values['miniorange_radius_auto_create_user'],
        );

        $utilities = new miniOrange_Radius_Utilities();
        $utilities->miniOrange_set_get_configurations( $variables_and_values, 'SET' );
        $utilities->miniOrange_status_message( 'Radius server configuration saved successfully.', 'status' );
    }

    function miniOrange_clear_configuration() {
        $variables_and_values = array(
            'miniorange_radius_enable_radius_login',
            'miniorange_radius_server_name',
            'miniorange_radius_server_ip_host',
            'miniorange_radius_radius_port',
            'miniorange_radius_shared_secret',
            'miniorange_radius_authentication_scheme',
            'miniorange_radius_attribute_mapping',
            'miniorange_radius_auto_create_user',
        );
        $utilities = new miniOrange_Radius_Utilities();
        $utilities->miniOrange_set_get_configurations( $variables_and_values, 'CLEAR' );
        $utilities->miniOrange_status_message( 'Radius server configuration Cleared successfully.', 'status' );
    }
}