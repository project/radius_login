<?php

namespace Drupal\radius_login\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\radius_login\miniOrange_Radius_Utilities;

class RadiusSupportForm extends FormBase
{
    public function getFormId() {
        return 'miniorange_radius_support_form';
    }

    public function buildForm( array $form, FormStateInterface $form_state ) {
        $utilities = new miniOrange_Radius_Utilities();
        $mo_asterisk = '<span style="color: red"><strong> *</strong></span>';
        $variables_and_values = array(
            'miniorange_radius_customer_admin_email',
            'miniorange_radius_customer_admin_phone',
        );
        $mo_db_values = $utilities->miniOrange_set_get_configurations( $variables_and_values, 'GET' );
        $miniOrnage_heading = t('Support');
        $form['miniorange_markup_radius_support_header'] = array(
            '#markup' => '<div class="miniorange_radius_table_layout_main"><div class="miniorange_radius_table_layout_sub miniorange_radius_container">'
        );

        /**
         * Support form start
         */
        $form['miniorange_vertical_tabs'] = array(
            '#type' => 'vertical_tabs',
            '#default_tab' => 'edit-publication',
        );
        $form['miniorange_support'] = array (
            '#type' => 'details',
            '#title' => t('Support'),
            '#group' => 'miniorange_vertical_tabs',
        );

        $form['miniorange_support']['miniorange_radius_support_email'] = array(
            '#type' => 'email',
            '#title' => t('Email' . $mo_asterisk ),
            '#default_value' => $mo_db_values['miniorange_radius_customer_admin_email'],
            '#attributes' => array( 'placeholder' => t('Enter your email address' ), 'style' => 'width:95%' ),
            '#prefix' => '<h2>'.$miniOrnage_heading.'</h2><hr><br>',
        );

        $form['miniorange_support']['miniorange_radius_support_phone'] = array(
            '#type' => 'textfield',
            '#title' => t('Phone Number'),
            '#default_value' => $mo_db_values['miniorange_radius_customer_admin_phone'],
            '#attributes' => array( 'placeholder' => t('Enter your Phone Number' ), 'style' => 'width:95%' ),
        );

        $form['miniorange_support']['miniorange_radius_support_query'] = array(
            '#type' => 'textarea',
            '#title' => t('Query' . $mo_asterisk ),
            '#attributes' => array( 'style' => 'width:95%', 'placeholder' => t('Write your query here' ) ),
            //'#required' => TRUE
        );

        $form['miniorange_support']['miniorange_markup_radius_support_submit'] = array(
            '#type' => 'submit',
            '#button_type' => 'primary',
            '#value' => t('Submit query'),
            '#prefix' => '<br>',
        );


        $form['miniorange_support']['miniorange_radius_support_note'] = array(
            '#markup' => t('<br><br><br><br><div>If you want custom features in the module, just drop an email to <a href="mailto:drupalsupport@xecurify.com">drupalsupport@xecurify.com</a></div>
                          <br>')
        );

        /**
         * Demo request form start
         */
        $miniOrnage_demo_request = t('Request for Demo');
        $miniOrnage_demo_request_description = t('Want to know about how the licensed module works? Let us know and we will arrange a demo for you.');
        $form['miniorange_demo_request'] = array (
            '#type' => 'details',
            '#title' => t('Request for Demo'),
            '#group' => 'miniorange_vertical_tabs',
        );

        $form['miniorange_demo_request']['miniorange_radius_demo_request_email'] = array(
            '#type' => 'email',
            '#title' => t('Email' . $mo_asterisk ),
            '#default_value' => $mo_db_values['miniorange_radius_customer_admin_email'],
            '#attributes' => array('placeholder' => t('Enter your email address' ), 'style' => 'width:95%;' ),
            '#prefix' => '<h2>'.$miniOrnage_demo_request.'</h2><hr><p class="mo_radius_highlight_background_note" >'. $miniOrnage_demo_request_description .'</p>',
            '#prefix' => '<h2>'.$miniOrnage_demo_request.'</h2><hr><p class="mo_radius_highlight_background_note" >'. $miniOrnage_demo_request_description .'</p>',
        );

        $form['miniorange_demo_request']['miniorange_radius_demo_request_plans'] = array(
            '#type' => 'select',
            '#title' => t('Plan/Version'),
            '#options' => array (
                'Drupal 8 RADIUS Client Premium Module' => t('Drupal 8 RADIUS Client Premium Module'),
                'Drupal 8 RADIUS Client Premium + Network Security Premium Module' => t('Drupal 8 RADIUS Client Premium + Network Security Premium Module'),
                'Not Sure' => t('Not Sure, need help for selecting plan.'),
            ),
            '#attributes' => array( 'style' => 'width:95%; height:30px'),
        );

        $form['miniorange_demo_request']['miniorange_radius_demo_request_usecase'] = array(
            '#type' => 'textarea',
            '#title' => t('Usecase' . $mo_asterisk ),
            '#attributes' => array( 'style' => 'width:95%', 'placeholder' => t('Please describe your use-case' ) ),
        );

        $form['miniorange_demo_request']['miniorange_markup_radius_demo_request_submit'] = array(
            '#type' => 'submit',
            '#button_type' => 'primary',
            '#value' => t('Submit Request'),
            '#prefix' => '<br>',
        );

        $form['miniorange_demo_request']['miniorange_radius_radius_demo_request_note'] = array(
            '#markup' => t('<br><br><br><br><div>If you are not sure with which plan you should go with, get in touch with us on <a href="mailto:drupalsupport@xecurify.com">drupalsupport@xecurify.com</a> and we will assist you with the suitable plan. </div>
                          <br>')
        );

        $form['miniorange_support_tab_end'] = array(
            '#markup' => '</div>',
        );

        $utilities->miniOrange_advertise_network_security($form, $form_state);

        return $form;
    }

    function submitForm( array &$form, FormStateInterface $form_state ) {
        $utilities = new miniOrange_Radius_Utilities();
        $form_values = $form_state->getValues();
        if( $form_values['miniorange_vertical_tabs__active_tab'] === 'edit-miniorange-support' ) {
            if ( $form_values['miniorange_radius_support_email'] == '' || $form_values['miniorange_radius_support_query'] == '' ) {
                miniOrange_Radius_Utilities::miniOrange_status_message('The<strong> Email </strong>and <strong>Query</strong> fields are mandatory.', 'error' );
                return;
            }
            $values = array(
                'email'            => $form_values['miniorange_radius_support_email'],
                'phone_or_plan'    => $form_values['miniorange_radius_support_phone'],
                'query_or_usecase' => $form_values['miniorange_radius_support_query'],
            );
            $utilities->miniOrange_send_support_query( $values );

        }else {
            if ( $form_values['miniorange_radius_demo_request_email'] == '' || $form_values['miniorange_radius_demo_request_usecase'] == '' ) {
                miniOrange_Radius_Utilities::miniOrange_status_message('The<strong> Email </strong>and <strong>Usecase</strong> fields are mandatory.', 'error' );
                return;
            }
            $values = array(
                'email'             => $form_values['miniorange_radius_demo_request_email'],
                'phone_or_plan'     => $form_values['miniorange_radius_demo_request_plans'],
                'query_or_usecase'  => $form_values['miniorange_radius_demo_request_usecase'],
                'demo_request'      => 'DEMO',
            );
            $utilities->miniOrange_send_support_query( $values );
        }
    }
}