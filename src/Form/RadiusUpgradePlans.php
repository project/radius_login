<?php

namespace Drupal\radius_login\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\radius_login\miniOrange_Radius_Utilities;

class RadiusUpgradePlans extends FormBase
{
    public function getFormId() {
        return 'miniorange_radius_upgrade_plans_form';
    }

    public function buildForm( array $form, FormStateInterface $form_state ) {

        $form['miniorange_radius_update_plans'] = array(
            '#markup' =>t('<div class="miniorange_radius_table_layout_main"><div class="miniorange_radius_table_layout_sub"><br><h2>&emsp; Upgrade Plans</h2><hr><br>'),
        );

        $form['markup_free'] = array(
            '#markup' => '<html lang="en">
                <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <!-- Main Style -->
                </head>
                <body>
                    <!-- Pricing Table Section -->
                    <section id="mo_pricing-table">
                        <div class="mo_container_1">
                            <div class="row">
                                <div class="pricing">
                                    <div class="mo_pricing-table mo_class_inline">
                                        <div class="pricing-header">
                                            <p class="pricing-title">Radius Client Free</p><br>
                                            <p class="pricing-rate"><sup>$</sup> 0<br><span class="miniorange_radius_one_time">[ One Time Payment ]</span></p>
                                             <a class="mobtn mo_btn_danger mo_btn_sm" style="padding:5px;">You are on this plan</a><br><br>
                                        </div>

                                        <div class="pricing-list">
                                            <ul>
                                                <li><strong>Authentication Protocol</strong></li>
                                                <li>PAP Authentication</li>
                                                <li>-</li>
                                                <li>-</li>
                                                <li>-</li>
                                                <li></li>
                                                <li><strong>Attribute Mapping For Authentication</strong></li>
                                                <li>Username</li>
                                                <li>-</li>
                                                <li>-</li>
                                                <li></li>
                                                <li><strong>Auto Create User after Successful Authentication</strong></li>
                                                <li>Allowed</li>
                                                <li></li>
                                                <li>-</li>
                                                <li></li>
                                                <li><strong>Support</strong></li>
                                                <li>Basic Email Support</li>
                                                <li><a href="'.miniOrange_Radius_Utilities::miniOrange_get_constants('SUPPORT_TAB').'">Contact us</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="mo_pricing-table mo_class_inline">
                                        <div class="pricing-header">
                                            <p class="pricing-title">Radius Client Premium</p><br>
                                            <p class="pricing-rate"><sup>$</sup> 199*<br><span class="miniorange_radius_one_time">[ One Time Payment ]</span></p>
                                             <a class="mobtn mo_btn_danger mo_btn_sm" target="_blank" style="padding:5px;" href="https://login.xecurify.com/moas/login?redirectUrl=https://login.xecurify.com/moas/initializepayment&requestOrigin=drupal_radius_client_premium_plan">Upgrade Now</a>*<br><br></h4>
                                        </div>
                                        <div class="pricing-list">
                                            <ul>
                                                <li><strong>Authentication Protocol</strong></li>
                                                <li>PAP Authentication</li>
                                                <li>CHAP-MD5 Authentication</li>
                                                <li>MSCHAP v1 Authentication</li>
                                                <li>EAP-MSCHAP v2 Authentication</li>
                                                <li></li>
                                                <li><strong>Attribute Mapping For Authentication</strong></li>
                                                <li>Username</li>
                                                <li>Email</li>
                                                <li>Both (we will try with username first)</li>
                                                <li></li>
                                                <li><strong>Auto Create User after Successful Authentication</strong></li>
                                                <li>Allowed</li>
                                                <li></li>
                                                <li>End to End Radius Server Integration *</li>
                                                <li></li>
                                                <li><strong>Support</strong></li>
                                                <li>Premium GoToMeeting Support</li>
                                                <li><a href="'.miniOrange_Radius_Utilities::miniOrange_get_constants('SUPPORT_TAB').'">Contact us</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="mo_pricing-table mo_class_inline">
                                        <div class="pricing-header">
                                            <p class="pricing-title">Radius Client Premium + Website Security Premium</p><br>
                                            <p class="pricing-rate"><sup>$</sup> 299*<br><span class="miniorange_radius_one_time">[ One Time Payment ]</span></p>
                                             <a class="mobtn mo_btn_danger mo_btn_sm" style="padding:5px;" href="'.miniOrange_Radius_Utilities::miniOrange_get_constants('SUPPORT_TAB').'">Contact us</a>*<br><br></h4>
                                        </div>
                                        <div class="pricing-list">
                                            <ul>
                                                <li>Radius Client Premium module</li>
                                                <li>+</li>
                                                <li>Website Security Premium Module</li>
                                                <li><a href="https://plugins.miniorange.com/drupal-web-security-pro">Features of Website Security</a></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li>End to End Radius Server Integration *</li>
                                                <li></li>
                                                <li><strong>Support</strong></li>
                                                <li>Premium GoToMeeting Support</li>
                                                <li><a href="'.miniOrange_Radius_Utilities::miniOrange_get_constants('SUPPORT_TAB').'">Contact us</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                    </div>
                </section>
                <!-- Pricing Table Section End -->
            </body>
            </html>',
        );

        $form['markup_1'] = array(
            '#markup' => t('<div style="margin: 0% 4% 2% 2%; text-align: justify;"><h3>The acronyms for Radius Authentication Protocol types:</h3>
                     <ul>
                         <li><strong>PAP:</strong> Password Authentication Protocol</li>
                         <li><strong>CHAP:</strong> Challenge Handshake Authentication Protocol</li>
                         <li><strong>MSCHAP:</strong> Microsoft Challenge Handshake Authentication Protocol</li>
                         <li><strong>EAP-MSCHAP:</strong> Extensible Authentication Protocol - Microsoft Challenge Handshake Authentication Protocol</li>
                     </ul>
                 </div>')
        );

        $form['markup_2'] = array(
            '#markup' => t('<div style="margin: 0% 4% 2% 2%; text-align: justify;">
                 * Cost applicable for one instance only. Licenses are perpetual and the Support Plan includes 12 months of maintenance (support and version updates). You can renew maintenance after 12 months at 50% of the current license cost.
                   </div>')
        );

        $form['markup_3'] = array(
            '#markup' => t('<div style="margin: 0% 4% 2% 2%; text-align: justify;"><h3>Return Policy - </h3>
                   At miniOrange, we want to ensure you are 100% happy with your purchase.
                   If the module you purchased is not working as advertised and you\'ve attempted to resolve any issues with our support team,
                   which couldn\'t get resolved, we will refund the whole amount given that you have a raised a refund request within the first 10 days of the purchase.
                   Please email us at <a href="mailto:drupalsupport@xecurify.com">drupalsupport@xecurify.com</a> for any queries regarding the return policy.
                   </div>')
        );

        $form['markup_4'] = array(
            '#markup' => t('<div style="margin: 0% 4% 5% 2%; text-align: justify;"><h3>* End to End Radius Server Integration (additional charges applied)</h3>
                 We will setup a Conference Call / Gotomeeting and do end to end configuration for you to setup Drupal as Radius Client.
                 We provide services to do the configuration on your behalf.
                 If you have any doubts regarding the upgrade plans, you can mail us at <a href="mailto:drupalsupport@xecurify.com"><i>drupalsupport@xecurify.com</i></a> or submit a query using the <a href="'. miniOrange_Radius_Utilities::miniOrange_get_constants('SUPPORT_TAB' ) . '">support form</a>.<br><br></div></div></div></div>')
        );

        return $form;
    }

    function submitForm( array &$form, FormStateInterface $form_state ) {

    }
}