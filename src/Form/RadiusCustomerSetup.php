<?php

namespace Drupal\radius_login\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\radius_login\miniOrange_Radius_Customer;
use Drupal\radius_login\miniOrange_Radius_Utilities;

class RadiusCustomerSetup extends FormBase
{
    public function getFormId() {
        return 'miniorange_radius_customer_setup_form';
    }

    public function buildForm( array $form, FormStateInterface $form_state ) {

        $utilities = new miniOrange_Radius_Utilities();

        $variables_and_values = array(
            'miniorange_radius_status',
            'miniorange_radius_customer_admin_email',
        );

        $mo_db_values = $utilities->miniOrange_set_get_configurations( $variables_and_values, 'GET' );
        $current_status = $mo_db_values['miniorange_radius_status'];

        $form['miniorange_markup_radius_register_header'] = array(
            '#markup' => '<div class="miniorange_radius_table_layout_main"><div class="miniorange_radius_table_layout_sub miniorange_radius_container">'
        );

        if ( $current_status === 'VALIDATE_OTP' ) {

            /**
             * Create container to hold @RadiuaRegisterLogin form elements.
             */
            $form['mo_radius_validate_otp_form'] = array(
                '#type' => 'fieldset',
                '#title' => t( 'Validate OTP' ),
                '#attributes' => array( 'style' => 'padding:2% 2% 30% 2%; margin-bottom:2%' ),
            );

            $form['mo_radius_validate_otp_form']['miniorange_radius_customer_otp_token'] = array(
                '#type' => 'textfield',
                '#title' => t('Enter OTP'),
                '#description' => t('<strong>Note:</strong> We have sent an OTP to <strong><em>'. $mo_db_values['miniorange_radius_customer_admin_email'] .'</em></strong>. Please enter the OTP to verify your email.'),
                '#maxlength' => 6,
                '#required' => TRUE,
                '#prefix' => '<br><hr><br>',
                '#suffix' => '<br>',
            );

            $form['mo_radius_validate_otp_form']['miniorange_radius_customer_validate_otp_button'] = array(
                '#type' => 'submit',
                '#button_type' => 'primary',
                '#value' => t('Validate OTP'),
                '#submit' => array('::miniorange_radius_validate_otp_submit'),
            );

            $form['mo_radius_validate_otp_form']['miniorange_radius_customer_setup_resendotp'] = array(
                '#type' => 'submit',
                '#value' => t('Resend OTP'),
                '#limit_validation_errors' => array(),
                '#submit' => array('::miniorange_radius_resend_otp'),
            );

            $form['mo_radius_validate_otp_form']['miniorange_radius_customer_setup_back'] = array(
                '#type' => 'submit',
                '#button_type' => 'danger',
                '#value' => t('BACK'),
                '#limit_validation_errors' => array(),
                '#attributes' => array( 'style' => 'margin-bottom:35%' ),
                '#submit' => array('::miniorange_radius_back'),
                '#suffix' => '</div>'
            );

            $utilities->miniOrange_advertise_network_security($form, $form_state);

            return $form;

        } elseif ( $current_status === 'PLUGIN_CONFIGURATION' ) {

            $variables_and_values = array(
                'miniorange_radius_customer_admin_email',
                'miniorange_radius_customer_id',
                'miniorange_radius_customer_admin_token',
                'miniorange_radius_customer_api_key',
            );

            $mo_db_values = $utilities->miniOrange_set_get_configurations( $variables_and_values, 'GET' );

            $miniOrnage_heading = t( 'Thank you for registering with miniOrange' );
            $form['miniorange_radius_profile'] = array(
                '#markup' => '<div class="mo_radius_welcome_message">'.$miniOrnage_heading.'</div><br><br>
                                  <h4>Your Profile: </h4>'
            );

            $header = array(
                'email' => array('data' => t('Customer Email')),
                'customerid' => array('data' => t('Customer ID')),
                'token' => array('data' => t('Token Key')),
                'apikey' => array('data' => t('API Key')),
            );

            $options = [];
            $options[0] = array(
                'email'      => $mo_db_values['miniorange_radius_customer_admin_email'],
                'customerid' => $mo_db_values['miniorange_radius_customer_id'],
                'token'      => $mo_db_values['miniorange_radius_customer_admin_token'],
                'apikey'     => $mo_db_values['miniorange_radius_customer_api_key'],
            );

            $form['fieldset']['customerinfo'] = array(
                '#theme' => 'table',
                '#header' => $header,
                '#rows' => $options,
                '#attributes' => array( 'style' => 'margin-bottom:35%' ),
                '#suffix' => '</div>'
            );

            $utilities->miniOrange_advertise_network_security($form, $form_state);

            return $form;
        }

        /**
         * Create container to hold @RadiuaRegisterLogin form elements.
         */
        $form['mo_radius_register_login_form'] = array(
            '#type' => 'fieldset',
            '#title' => t( 'Register/Login with miniOrange' ),
            '#attributes' => array( 'style' => 'padding:2% 2% 6% 2%; margin-bottom:2%' ),
        );

        $form['mo_radius_register_login_form']['miniorange_radius_register_message'] = array(
            '#markup' =>'<br><hr><h3>Why should I register?</h3><div class="mo_radius_highlight_background_note">You should register so that in case you need help, we can help you with step by step instructions.
                    <b>You will also need a miniOrange account to upgrade to the premium version of the module.</b> 
                    We do not store any information except the email that you will use to register with us.Please enter a valid email id that you have access to. We will send OTP to this email for verification.</div><br>'
        );
        $form['mo_radius_register_login_form']['miniorange_radius_login_message'] = array(
            '#markup' => '<div class="mo_radius_highlight_background_note">If you face any issues during registration then you can <b><a href="https://www.miniorange.com/businessfreetrial" target="_blank">click here</a></b> to register and use the same credentials below to login into the module.</div><br>',
        );

        $form['mo_radius_register_login_form']['miniorange_radius_customer_register_username'] = array(
            '#type' => 'email',
            '#title' => t('Email'),
            '#description' => t('<b>Note:</b> Use valid EmailId. (We discourage the use of disposable emails)'),
            '#required' => TRUE,
        );

        $form['mo_radius_register_login_form']['miniorange_radius_customer_register_phone'] = array(
            '#type' => 'textfield',
            '#title' => t('Phone'),
            '#description' => t('<b>Note:</b> We will only call if you need support.' ),
        );

        $form['mo_radius_register_login_form']['miniorange_radius_customer_register_password'] = array(
            '#type' => 'password_confirm',
            '#required' => TRUE,
        );

        $form['mo_radius_register_login_form']['miniorange_radius_customer_register_submit'] = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
            '#button_type' => 'primary',
            '#attributes' => array( 'style' => 'margin:2% 0' ),
            '#suffix' => '</div>',
        );

        $utilities->miniOrange_advertise_network_security($form, $form_state);

        return $form;
    }

    function submitForm( array &$form, FormStateInterface $form_state ) {
        $utilities = new miniOrange_Radius_Utilities();
        $form_values = $form_state->getValues();
        $username = $form_values['miniorange_radius_customer_register_username'];
        $phone    = $form_values['miniorange_radius_customer_register_phone'];
        $password = $form_values['miniorange_radius_customer_register_password'];

        $customer_config = new miniOrange_Radius_Customer( $username, $phone, $password, NULL );
        $check_customer_response = json_decode( $customer_config->checkCustomer() );

        if ( $check_customer_response->status == 'TRANSACTION_LIMIT_EXCEEDED' ) {
            $utilities->miniOrange_add_loggers( $check_customer_response->message, 'ERROR' );
            $utilities->miniOrange_status_message( 'An error has been occured. Please Try after some time or 
                        <a href="mailto:  drupalsupport@xecurify.com"><i>contact us</i></a>. Click <a href="' . $utilities->miniOrange_get_constants('LOGS' ) . '"><i>here</i></a> for more details.', 'error' );
            return;
        }
        if ( $check_customer_response->status == 'CUSTOMER_NOT_FOUND' ) {
            /**
             * Save customer data to database
             */
            $variables_and_values = array(
                'miniorange_radius_customer_admin_email'    => $username,
                'miniorange_radius_customer_admin_phone'    => $phone,
                'miniorange_radius_customer_admin_password' => $password,
            );
            $utilities->miniOrange_set_get_configurations( $variables_and_values, 'SET' );
            $send_otp_response = json_decode( $customer_config->sendOtp() );
            if ( $send_otp_response->status == 'SUCCESS' ) {
                $variables_and_values = array(
                    'miniorange_radius_tx_id'   => $send_otp_response->txId,
                    'miniorange_radius_status'  => 'VALIDATE_OTP',
                );
                $utilities->miniOrange_set_get_configurations( $variables_and_values, 'SET' );
                $utilities->miniOrange_status_message( 'Verify email address by entering the passcode sent to '.$username, 'status' );
            } else {
                $utilities->miniOrange_status_message( 'An error has been occurred. Please try after some time.' , 'error' );
                return;
            }
        } elseif ( $check_customer_response->status == 'CURL_ERROR' ) {
            $utilities->miniOrange_status_message( 'cURL is not enabled. Please enable cURL' , 'error' );
            return;
        } else {
            $customer_keys_response = json_decode( $customer_config->getCustomerKeys() );
            if ( json_last_error() == JSON_ERROR_NONE ) {
                $variables_and_values = array(
                    'miniorange_radius_customer_id'            => $customer_keys_response->id,
                    'miniorange_radius_customer_admin_token'   => $customer_keys_response->token,
                    'miniorange_radius_customer_admin_email'   => $username,
                    'miniorange_radius_customer_admin_phone'   => $phone,
                    'miniorange_radius_customer_api_key'       => $customer_keys_response->apiKey,
                    'miniorange_radius_status'                 => 'PLUGIN_CONFIGURATION',
                );
                $utilities->miniOrange_set_get_configurations( $variables_and_values, 'SET' );
                $utilities->miniOrange_status_message( 'Your account retrieved successfully.' , 'status' );
                return;

            } else {
                $utilities->miniOrange_status_message( 'Invalid credentials' , 'error' );
                return;
            }
        }
    }

    /**
     * Validate OTP.
     * @param array $form
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     */
    public function miniorange_radius_validate_otp_submit( array &$form, FormStateInterface $form_state ) {
        $utilities = new miniOrange_Radius_Utilities();
        $form_values = $form_state->getValues();
        $otp_token = trim( $form_values['miniorange_radius_customer_otp_token'] );
        $variables_and_values = array(
            'miniorange_radius_tx_id',
            'miniorange_radius_customer_admin_email',
            'miniorange_radius_customer_admin_phone',
            'miniorange_radius_customer_admin_password'

        );
        $mo_db_values = $utilities->miniOrange_set_get_configurations( $variables_and_values, 'GET' );

        $username = $mo_db_values['miniorange_radius_customer_admin_email'];
        $tx_id    = $mo_db_values['miniorange_radius_tx_id'];
        $phone    = $mo_db_values['miniorange_radius_customer_admin_phone'];

        $customer_config = new miniOrange_Radius_Customer( $username, $phone, NULL, $otp_token );
        $validate_otp_response = json_decode( $customer_config->validateOtp( $tx_id ) );

        if ( $validate_otp_response->status == 'SUCCESS' ) {
            $utilities->miniOrange_set_get_configurations( array( 'miniorange_radius_tx_id' ), 'CLEAR' );
            $password = $mo_db_values['miniorange_radius_customer_admin_password'];
            $customer_config = new miniOrange_Radius_Customer( $username, $phone, $password, NULL );
            $create_customer_response = json_decode( $customer_config->createCustomer() );

            if ( $create_customer_response->status == 'SUCCESS' ) {
                $variables_and_values = array(
                    'miniorange_radius_status'               => 'PLUGIN_CONFIGURATION',
                    'miniorange_radius_customer_admin_email' => $username,
                    'miniorange_radius_customer_admin_phone' => $phone,
                    'miniorange_radius_customer_admin_token' => $create_customer_response->token,
                    'miniorange_radius_customer_id'          => $create_customer_response->id,
                    'miniorange_radius_customer_api_key'     => $create_customer_response->apiKey,
                );
                $utilities->miniOrange_set_get_configurations( $variables_and_values, 'SET' );
                $utilities->miniOrange_status_message('Your account has been created successfully!', 'status' );
                return;

            } else if (trim($create_customer_response->status) == 'INVALID_EMAIL_QUICK_EMAIL') {
                $utilities->miniOrange_status_message('There was an error creating an account for you. You may have entered an invalid Email-Id
                                                               <strong>(We discourage the use of disposable emails) </strong><br>Please try again with a valid email.', 'error' );
                return;
            } else {
                $utilities->miniOrange_status_message('There was an error while creating customer. Please try after some time.', 'error' );
                return;
            }
        } else {
            $utilities->miniOrange_status_message('Invalid  OTP', 'error');
           return;
        }
    }

    public function miniorange_radius_resend_otp () {
        $utilities = new miniOrange_Radius_Utilities();
        $utilities->miniOrange_set_get_configurations( array( 'miniorange_radius_tx_id' ), 'CLEAR' );
        $variables_and_values = array(
            'miniorange_radius_customer_admin_email',
            'miniorange_radius_customer_admin_phone',
        );
        $mo_db_values = $utilities->miniOrange_set_get_configurations( $variables_and_values, 'GET' );

        $username = $mo_db_values['miniorange_radius_customer_admin_email'];
        $phone    = $mo_db_values['miniorange_radius_customer_admin_phone'];

        $customer_config = new miniOrange_Radius_Customer( $username, $phone, NULL, NULL );
        $send_otp_response = json_decode( $customer_config->sendOtp() );
        if ( $send_otp_response->status == 'SUCCESS' ) {
            // Store txID.
            $variables_and_values = array(
                'miniorange_radius_tx_id' => $send_otp_response->txId,
                'miniorange_radius_status' => 'VALIDATE_OTP'
            );
            $utilities->miniOrange_set_get_configurations( $variables_and_values, 'SET' );
            $utilities->miniOrange_status_message('Verify email address by entering the passcode sent to '. $username , 'status');
            return;
        }else{
            $utilities->miniOrange_status_message('An error has been occured. Please try after some time' , 'error');
            return;
        }
    }

    public function miniorange_radius_back() {
        $utilities = new miniOrange_Radius_Utilities();
        $variables_and_values = array(
            'miniorange_radius_customer_admin_email',
            'miniorange_radius_customer_admin_phone',
            'miniorange_radius_tx_id',
        );
        $utilities->miniOrange_set_get_configurations( $variables_and_values, 'CLEAR' );
        $utilities->miniOrange_set_get_configurations( array( 'miniorange_radius_status' => 'CUSTOMER_SETUP' ), 'SET' );
        $utilities->miniOrange_status_message('Register/Login with your miniOrange Account', 'status' );
        return;
    }
}