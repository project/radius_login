<?php
use Drupal\Core\Form\FormStateInterface;
/**
 * @file
 * Module file for miniOrange ldap Module.
 */

use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\radius_login\RadiusLibraries\Radius;
use Drupal\radius_login\miniOrange_Radius_Utilities;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Add CSS for entire module
 */
function radius_login_page_attachments( array &$attachments ) {
    $attachments['#attached']['library'][] = 'radius_login/radius_login.admin';
}

function radius_login_form_alter( &$form, FormStateInterface $form_state, $form_id ) {

    /**
     * Disable few of the radio buttons from radio button group.
     */
    if ( $form_id == 'miniorange_radius_config_form' ) {
        $form['mo_radiua_server_configurations_form']['miniorange_radius_authentication_scheme']['chap_md5_authentication']      = [ '#disabled' => TRUE ];
        $form['mo_radiua_server_configurations_form']['miniorange_radius_authentication_scheme']['mschap_v1_authentication']     = [ '#disabled' => TRUE ];
        $form['mo_radiua_server_configurations_form']['miniorange_radius_authentication_scheme']['eap_mschap_v2_authentication'] = [ '#disabled' => TRUE ];

        $form['mo_radiua_server_configurations_form']['miniorange_radius_attribute_mapping']['email']        = [ '#disabled' => TRUE ];
        $form['mo_radiua_server_configurations_form']['miniorange_radius_attribute_mapping']['both']         = [ '#disabled' => TRUE ];

        $form['mo_radiua_server_configurations_form']['miniorange_radius_auto_create_user']['not_allowed']   = [ '#disabled' => TRUE ];
    }

    $utilities = new miniOrange_Radius_Utilities();
    $mo_db_values = $utilities->miniOrange_set_get_configurations( array( 'miniorange_radius_enable_radius_login' ), 'GET' );
	$enable_radius_login = $mo_db_values['miniorange_radius_enable_radius_login'];

    /**
     * Check whether user is logged in or not. If not the condition is TRUE.
     */

	if( !\Drupal::currentUser()->isAuthenticated() ) {
		if ( $form_id == 'user_login_block' || $form_id == 'user_login' || $form_id == 'user_login_form' ) {
			if( $enable_radius_login ) {
                /**
                 * Disable default authentication of the Drupal and add manual validation by calling function
                 */
                $form['#validate'][] = 'radius_login_form_alter_submit';
			}
		}
	}
}

function radius_login_form_alter_submit( &$form, FormStateInterface $form_state ) {
	$username = $form_state->getValue('name' );
	$password = $form_state->getValue('pass' );

    $auth_response = miniorange_radius_login( $username, $password );

    if ( is_array( $auth_response ) && isset( $auth_response[0] ) && $auth_response[0] === 'error' ) {
        $form_state->setError($form, t( $auth_response[1] ));
        return ;
    }

    if( is_object( $auth_response ) && isset( $auth_response->uid->value ) ) {
        /** Create session and Login the user */
        user_login_finalize( $auth_response );

        $response = new RedirectResponse( miniOrange_Radius_Utilities::miniOrange_get_constants('BASE_URL') );
        $request  = \Drupal::request();
        $request->getSession()->save();
        $response->prepare($request);
        \Drupal::service('kernel')->terminate($request, $response);
        $response->send();exit();
        return new Response();

    } else {
        $form_state->setError($form, t( 'Something went wrong. Please try again later.' ));
        return ;
    }
}

function miniorange_radius_login( $username, $password ) {

    $utilities = new miniOrange_Radius_Utilities();
    $variables_and_values = array(
        'miniorange_radius_server_ip_host',
        'miniorange_radius_radius_port',
        'miniorange_radius_shared_secret',
        'miniorange_radius_authentication_scheme',
        'miniorange_radius_attribute_mapping',
        'miniorange_radius_auto_create_user',
    );
    $mo_db_values = $utilities->miniOrange_set_get_configurations( $variables_and_values, 'GET' );

    $radius_host                  = $mo_db_values['miniorange_radius_server_ip_host'];
    $radius_port                  = $mo_db_values['miniorange_radius_radius_port'];
    $radius_shared_secret         = $mo_db_values['miniorange_radius_shared_secret'];
    $radius_authentication_scheme = $mo_db_values['miniorange_radius_authentication_scheme'];
    $user = FALSE;

    $radius = new Radius();
    $radius->setServer( $radius_host )
           ->setSecret( $radius_shared_secret )
           ->setAuthenticationPort( $radius_port )
           ->setNasIpAddress('127.0.0.1' )
           ->setAttribute(32, 'miniOrange-Drupal' );

    switch ( $radius_authentication_scheme ) {
        case 'pap_authentication':
            $response = pap_authentication( $username, $password, $radius );
            break;
        default:
            $response = [ 'error', 'Invalid authentication scheme.' ];
    }

    if ( isset( $response[0] ) && $response[0] === 'error' ) {
        return $response;

    } elseif ( $response === true ) {
        $user = user_load_by_name( $username );
        /**
         * Create user if user does not exist in Drupal site.
         */
        if( !$user ) {
            $random_password = \Drupal::service('password_generator')->generate($length = 10);
            $new_user = array (
                'name' => $username,
                'mail' => $username,
                'pass' => $random_password,
                'status' => 1,
            );
            $account = User::create( $new_user );
            $account->save();
        }

        /** check if the user is blocked by admin. */
        if( user_is_blocked( $username ) ) {
            return [ 'error', 'Your account has been blocked. Please contact your admin.' ];
        }

        $user = user_load_by_name( $username );
    }
    return $user;
}

function pap_authentication( $username, $password, $radius ) {
    $response = $radius->accessRequest( $username, $password );
    if ( $response === false ) {
        // false returned on failure
        return [ 'error', 'Access-Request failed with error ' . $radius->getErrorMessage() . ' Error code: ' . $radius->getErrorCode() ];
    }
    return $response;
}

/**
 * Implements hook_help().
 */
function radius_login_help($route_name, RouteMatchInterface $route_match) {
    switch ( $route_name ) {
        case 'help.page.radius_login':
            $url = Url::fromRoute('user.admin_index')->toString();
            $moduleLink = $url .'/radius_login/radius_config';
            $supoortLink = $url .'/radius_login/radius_support';

            $output = '';
            $output .= '<h3>' . t('About <a target="_blank" href="https://plugins.miniorange.com/drupal-radius-login">[Know more]</a>') . '</h3>';
            $output .= '<p>' . t('RADIUS (Remote Authentication Dial-In User Service) Client or RADIUS Login module allows users to login with any RADIUS server. We support radius authentication schemes like PAP, CHAP (MD5), MS-CHAP V1, EAP-MSCHAP v2 and other schemes on request.') . '</p>';
            $output .= '<h3>' . t('Configuration') . '</h3>';
            $output .= '<p>' . t('Configure Radius Login Client in Configuration » <a target = "_blank" href=" ' . $url . ' ">People</a> » <a target = "_blank" href=" ' . $moduleLink . ' ">Radius Login Client</a>:') . '</p>';
            $output .= '<p>' . t('If you need any assistance, go to <a target = "_blank" href="'. $supoortLink .'">Support tab</a> and submit your query. You can also email us at <a href="mailto:drupalsupport@xecurify.com">drupalsupport@xecurify.com</a> or <a href="mailto:info@xecurify.com">info@xecurify.com</a>') . '</p>';
            return $output;
    }
}